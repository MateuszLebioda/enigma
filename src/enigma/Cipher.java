package enigma;
public class Cipher {
    
    Cipher(Alphabet alphabet){
        this.alphabet = alphabet;
    }
    
    private int key = 1;
    private Alphabet alphabet;
    
    public void setKey(int key){
        this.key = key;
    }
    public void setAlphabet(Alphabet alphabet){
        this.alphabet = alphabet;
    }
    String encript(String in){
        StringBuilder out = new StringBuilder();
        if(!alphabet.isTextValid(in)){
           throw new IllegalArgumentException("Tekst zawiera znaki niedozowlone"); 
        }
            for(int i=0, len = in.length();i<len;i++){
                    int ch = in.charAt(i);
                    int a = alphabet.indexOf(ch);
                    a=(a+key) % alphabet.length(); // zapetla sie 
                    ch = alphabet.charAt(a);        
                    out.append((char)ch);
            }
            return out.toString();
         
    }
}
