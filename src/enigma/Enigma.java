package enigma;
public class Enigma {
    public static void main(String[] args) {
        Alphabet alphabet = new Alphabet(Alphabet.ALPH_UP_WITH_SPACE);
        Cipher cipher = new Cipher(alphabet);
        System.out.println(cipher.encript("ALAMAKOTA"));
        System.out.println(cipher.encript("ALA MA KOTA"));
    } 
}
