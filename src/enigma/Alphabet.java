package enigma;
public class Alphabet {
    Alphabet(String alphabet){
        this.charSet = alphabet;
    }

    private final String charSet;
    public final static String ALPH_UP_WITH_SPACE = "ABCDEFGHIJKLMNOPRSTUWXYZ ";
    public final static String ALPH_DOWN_WITH_SPACE = "abcdefghijklmnoprstuwxyz ";
    public final static String ALPH_WITH_SPACE = "ABCDEFGHIJKLMNOPRSTUWXYZabcdefghijklmnoprstuwxyz ";
    public final static String NUMBERS = "1234567890 "; // DODAŁEM KOLEJNA OPCJE ALFABETU 

    boolean isTextValid(String charSet){
        return charSet.matches("^[" +this.charSet+ "]+$"); // wyrazenia regularne 
    }
    String getCharSet(){
        return charSet;
    }

    int indexOf(int ch) {
        return charSet.indexOf(ch);
    }

    int length() {
        return charSet.length();
    }

    int charAt(int a) {
        return charSet.charAt(a);
    }
}
